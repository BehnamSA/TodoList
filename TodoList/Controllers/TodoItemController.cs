﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Business.Common.Exceptions;
using TodoList.Business.Project;
using TodoList.Infrastructure.Util;
using TodoList.Models;

namespace TodoList.Controllers;

public class TodoItemController : AbstractController
{
    private readonly ProjectBusiness _projectBusiness;

    public TodoItemController(ProjectBusiness projectBusiness)
    {
        _projectBusiness = projectBusiness;
    }

    [HttpGet]
    public async Task<IActionResult> Index(int todoListId)
    {
        if (HasUserAuthenticated() is false)
            return RedirectToLoginView();

        throw new NotImplementedException();
        //
        // var todoList = await _projectBusiness.GetByIdAsync(todoListId);
        // var todoItems = await _projectBusiness.GetUserStoriesAsync(todoListId);
        //
        // return View(new TodoListWithItemsViewModel(todoList, todoItems));
    }

    [HttpGet]
    public async Task<IActionResult> GetTodoItem(int todoListId, int todoItemId)
    {
        if (HasUserAuthenticated() is false)
            return RedirectToLoginView();

        throw new NotImplementedException();
        //
        // var todoList = await _projectBusiness.GetByIdAsync(todoListId);
        // var todoItems = await _projectBusiness.GetUserStoriesAsync(todoListId);
        // var todoItem = await _projectBusiness.GetTodoItemAsync(todoItemId);
        //
        // return View("Index", new TodoListWithItemsViewModel(todoList, todoItems, UpdatingTodoItem: todoItem));
    }

    [HttpPost]
    public async Task<IActionResult> Create(int todoListId, TodoItemDto dto)
    {
        throw new NotImplementedException();

        // Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(dto.Title) is false);
        //
        // await _projectBusiness.AddTodoItemAsync(todoListId, new[] { dto });
        // return RedirectToIndex(todoListId);
    }

    [HttpPost]
    public async Task<IActionResult> MakeItDone(int todoListId, int todoItemId)
    {
        // await _projectBusiness.MakeTodoItemDone(todoListId, todoItemId);

        return RedirectToIndex(todoListId);
    }

    [HttpPost]
    public async Task<IActionResult> MakeItUndone(int todoListId, int todoItemId)
    {
        // await _projectBusiness.MakeTodoItemUndone(todoListId, todoItemId);

        return RedirectToIndex(todoListId);
    }

    [HttpPost]
    public async Task<IActionResult> Update(int todoListId, int todoItemId, TodoItemDto dto)
    {
        Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(dto.Title) is false);

        throw new NotImplementedException();

        // await _projectBusiness.UpdateTodoItemAsync(todoListId, todoItemId, dto);
        // return RedirectToIndex(todoListId);
    }

    [HttpPost]
    public async Task<IActionResult> Delete(int todoListId, int todoItemId)
    {
        throw new NotImplementedException();
        //
        // await _projectBusiness.DeleteTodoItemsAsync(todoListId, todoItemId);
        // return RedirectToIndex(todoListId);
    }

    private IActionResult RedirectToIndex(int todoListId) => RedirectToAction("Index", new { todoListId = todoListId });
}