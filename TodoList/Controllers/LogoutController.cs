﻿using Microsoft.AspNetCore.Mvc;

namespace TodoList.Controllers;

public class LogoutController : AbstractController
{
    [HttpPost]
    public IActionResult Logout()
    {
        DeleteUserIdCookie();
        return RedirectToAction(actionName: "Index", controllerName: "Login");
    }
}