﻿using Microsoft.AspNetCore.Mvc;

namespace TodoList.Controllers;

public abstract class AbstractController : Controller
{
    protected const string UserIdCookieKey = "UserId";

    protected int? GetUserIdFromCookie()
    {
        Request.Cookies.TryGetValue(UserIdCookieKey, out var userId);
        return string.IsNullOrWhiteSpace(userId) ? null : int.Parse(userId);
    }

    protected bool HasUserAuthenticated() => GetUserIdFromCookie() is not null;

    protected void SetUserIdCookie(int id)
    {
        var cookieBuilder = new CookieBuilder();
        cookieBuilder.Name = UserIdCookieKey;
        cookieBuilder.Expiration = DateTime.Now.AddDays(1).TimeOfDay;

        HttpContext.Response.Cookies.Append(key: cookieBuilder.Name, id.ToString());
        cookieBuilder.Build(HttpContext);
    }

    protected void DeleteUserIdCookie()
    {
        HttpContext.Response.Cookies.Delete(UserIdCookieKey);
    }

    protected IActionResult RedirectToLoginView() => RedirectToAction(actionName: "Index", controllerName: "Login");
}