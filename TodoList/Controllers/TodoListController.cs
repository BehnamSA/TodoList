﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Business.Common.Exceptions;
using TodoList.Business.Project;
using TodoList.Controllers.Exceptions;
using TodoList.Infrastructure.Util;

namespace TodoList.Controllers;

public class TodoListController : AbstractController
{
    private readonly ProjectBusiness _projectBusiness;

    public TodoListController(ProjectBusiness projectBusiness)
    {
        _projectBusiness = projectBusiness;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        if (HasUserAuthenticated() is false)
            return RedirectToLoginView();

        var userId = GetUserIdFromCookie()!.Value;

        var viewModels = await _projectBusiness.GetAsync(userId);

        return View(viewModels);
    }

    [HttpPost]
    public async Task<IActionResult> Create(string title)
    {
        Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(title) is false);
        Guard.Assert<ContentInvalidLengthException>(title.Length <= 128);

        var userId = GetUserIdFromCookie()!;
        await _projectBusiness.CreateAsync(title, userId.Value);

        return RedirectToIndex();
    }

    [HttpPost]
    public async Task<IActionResult> Delete(int id)
    {
        await _projectBusiness.DeleteAsync(id);
        return RedirectToIndex();
    }


    private IActionResult RedirectToIndex() => RedirectToAction(nameof(Index));
}