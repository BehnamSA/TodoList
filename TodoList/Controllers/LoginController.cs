﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Business.Authentication;

namespace TodoList.Controllers;

public class LoginController : AbstractController
{
    private readonly AuthBusiness _authBusiness;

    public LoginController(AuthBusiness authBusiness)
    {
        _authBusiness = authBusiness;
    }

    [HttpGet]
    public IActionResult Index()
    {
        if (HasUserAuthenticated())
            return RedirectToTodoListIndex();

        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Login(string username, string password)
    {
        await _authBusiness.LoginAsync(username, password);

        var user = (await _authBusiness.GetUserAsync(username))!;

        SetUserIdCookie(user.Id);

        return RedirectToTodoListIndex();
    }


    private IActionResult RedirectToTodoListIndex()
        => RedirectToAction(actionName: "Index", controllerName: "TodoList");
}