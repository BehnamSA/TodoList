﻿namespace TodoList.Controllers.Exceptions;

public class ContentInvalidLengthException : Exception
{
    public ContentInvalidLengthException() : base(message: "Content length is invalid")
    {
    }
}