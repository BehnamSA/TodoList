﻿namespace TodoList.Controllers.Exceptions;

public class InvalidEmailAddressException : Exception
{
    public InvalidEmailAddressException() : base(message: "Email address is invalid")
    {
    }
}