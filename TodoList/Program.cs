using TodoList;
using TodoList.Business;
using TodoList.Infrastructure.Persistence;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews(options => { options.Filters.Add(typeof(ExceptionHandlerActionFilter)); });

DataAccessBootstrapper.Run(builder.Services);
BusinessBootstrapper.Run(builder.Services);

var app = builder.Build();

EnsureDatabaseIsCreated(app);

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Login}/{action=Index}/{id?}");

app.Run();

static void EnsureDatabaseIsCreated(WebApplication app)
{
    var scope = app.Services.CreateScope();
    var dbContext = scope.ServiceProvider.GetRequiredService<ProjectManagementDbContext>();
    dbContext.Database.EnsureCreated();
}