﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace TodoList;

public class ExceptionHandlerActionFilter : IActionFilter
{
    public void OnActionExecuting(ActionExecutingContext context)
    {
        SaveTodoListIdAndTodoItemIdCookies(context);
    }

    

    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (context.Exception is null) return;

        context.HttpContext.Request.Cookies.TryGetValue("todoListId", out var todoListId);
        context.HttpContext.Request.Cookies.TryGetValue("todoItemId", out var todoItemId);
        
        context.HttpContext.Response.Cookies.Delete("todoListId");
        context.HttpContext.Response.Cookies.Delete("todoItemId");

        var controller = (context.Controller as Controller)!;
        controller.TempData["ErrorMessage"] = context.Exception.Message;

        var descriptor = context.ActionDescriptor;
        var controllerName = descriptor.RouteValues["controller"]!;

        context.Result = new RedirectToActionResult(
            "Index", controllerName,
            new { todoListId = todoListId, todoItemId = todoItemId }
        );

        context.ExceptionHandled = true;
    }
    
    
    private void SaveTodoListIdAndTodoItemIdCookies(ActionExecutingContext context)
    {
        foreach (var keyValue in context.HttpContext.Request.Query)
        {
            switch (keyValue.Key.ToLower())
            {
                case "todolistid":
                {
                    var cookieBuilder = new CookieBuilder();
                    cookieBuilder.Name = "todoListId";
                    cookieBuilder.Expiration = DateTime.Now.AddYears(1).TimeOfDay;

                    context.HttpContext.Response.Cookies.Append(key: cookieBuilder.Name, keyValue.Value!);
                    cookieBuilder.Build(context.HttpContext);
                }; break;
                case "todoitemid":
                {
                    var cookieBuilder = new CookieBuilder();
                    cookieBuilder.Name = "todoItemId";
                    cookieBuilder.Expiration = DateTime.Now.AddYears(1).TimeOfDay;

                    context.HttpContext.Response.Cookies.Append(key: cookieBuilder.Name, keyValue.Value!);
                    cookieBuilder.Build(context.HttpContext);
                }; break;
                default: break;
            }
        }
    }
}