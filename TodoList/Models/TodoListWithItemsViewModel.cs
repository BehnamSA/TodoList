﻿using TodoList.Business.Project.ViewModels;

namespace TodoList.Models;

public record TodoListWithItemsViewModel(
    ProjectViewModel Project
    // UserStoryViewModel[] TodoItems,
    // UserStoryViewModel? UpdatingTodoItem = null
);