﻿using TodoList.Infrastructure.Persistence.Item;

namespace TodoList.Business.Item.ViewModels;

public record ItemViewModel
{
    public required int Id { get; set; }
    public required int UserStoryId { get; set; }
    public required int? BusinessValue { get; set; }
    public required string Title { get; set; }
    public required string? Description { get; set; }
    public required ItemStatus Status { get; set; }
    public required ItemType Type { get; set; }
    public required UserViewModel? User { get; set; }
    public required DiscussionViewModel[] Discussions { get; set; } = Array.Empty<DiscussionViewModel>();

    public record UserViewModel(int Id, string Username);

    public record DiscussionViewModel(int Id, string Message, DateTime CreateDate);
}