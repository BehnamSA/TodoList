﻿using Microsoft.EntityFrameworkCore;
using TodoList.Business.Common.Exceptions;
using TodoList.Business.Item.Dtos;
using TodoList.Business.Item.Exceptions;
using TodoList.Business.Item.Extensions;
using TodoList.Business.Item.ViewModels;
using TodoList.Business.User.Exceptions;
using TodoList.Business.UserStory.Exceptions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.Item;
using TodoList.Infrastructure.Persistence.User;
using TodoList.Infrastructure.Persistence.UserStory;
using TodoList.Infrastructure.Util;

namespace TodoList.Business.Item;

public class ItemBusiness
{
    private readonly ProjectManagementDbContext _dbContext;
    private readonly DbSet<ItemEntity> _itemRepository;
    private readonly DbSet<UserStoryEntity> _userStoryRepository;
    private readonly DbSet<UserEntity> _userRepository;

    public ItemBusiness(ProjectManagementDbContext dbContext)
    {
        _dbContext = dbContext;
        _itemRepository = dbContext.Set<ItemEntity>();
        _userStoryRepository = dbContext.Set<UserStoryEntity>();
        _userRepository = dbContext.Set<UserEntity>();
    }

    public async Task<int> CreateAsync(CreateItemDto dto)
    {
        Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(dto.Title) is false);

        Guard.Assert<UserStoryNotFoundException>(await _userStoryRepository.AnyAsync(us => us.Id == dto.UserStoryId));

        Guard.Assert<UserNotFoundException>(
            dto.UserId is null || await _userRepository.AnyAsync(u => u.Id == dto.UserId)
        );

        var entity = new ItemEntity
        {
            UserStoryId = dto.UserStoryId,
            AssignedToUserId = dto.UserId,
            Title = dto.Title,
            Description = dto.Description,
            Status = dto.Status,
            BusinessValue = dto.BusinessValue,
            Type = dto.Type,
        };

        _itemRepository.Add(entity);

        await _dbContext.SaveChangesAsync();

        return entity.Id;
    }

    public async Task<ItemViewModel?> GetAsync(int id)
    {
        var entity = await _itemRepository
            .Include(i => i.AssignedToUser).Include(i => i.DiscussionMessages)
            .SingleOrDefaultAsync(i => i.Id == id);

        if (entity is null) return null;

        return new ItemViewModel
        {
            Id = entity.Id,
            UserStoryId = entity.UserStoryId,
            Title = entity.Title,
            Description = entity.Description,
            BusinessValue = entity.BusinessValue,
            Status = entity.Status,
            Type = entity.Type,
            User = entity.AssignedToUser.ToViewModel(),
            Discussions = entity.DiscussionMessages
                .OrderBy(idm => idm.CreateDate)
                .Select(idm => new ItemViewModel.DiscussionViewModel(idm.Id, idm.Message, idm.CreateDate))
                .ToArray()
        };
    }

    public async Task UpdateAsync(int id, UpdateItemDto dto)
    {
        var entity = await _itemRepository.FindAsync(id);
        Guard.Assert<ItemNotFoundException>(entity is not null);

        Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(dto.Title) is false);

        Guard.Assert<UserNotFoundException>(
            dto.UserId is null || await _userRepository.AnyAsync(u => u.Id == dto.UserId)
        );

        entity.AssignedToUserId = dto.UserId;
        entity.Title = dto.Title;
        entity.Description = dto.Description;
        entity.Status = dto.Status;
        entity.BusinessValue = dto.BusinessValue;

        _itemRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id)
    {
        var entity = await _itemRepository.FindAsync(id);

        if (entity is null) return;

        _itemRepository.Remove(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateStatusAsync(int id, ItemStatus status)
    {
        var entity = await _itemRepository.FindAsync(id);
        Guard.Assert<ItemNotFoundException>(entity is not null);

        entity.Status = status;

        _itemRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }
}