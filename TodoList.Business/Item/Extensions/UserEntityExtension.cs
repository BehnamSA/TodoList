﻿using TodoList.Business.Item.ViewModels;
using TodoList.Infrastructure.Persistence.User;

namespace TodoList.Business.Item.Extensions;

internal static class UserEntityExtension
{
    internal static ItemViewModel.UserViewModel? ToViewModel(this UserEntity? entity)
    {
        return entity is null ? null : new ItemViewModel.UserViewModel(entity.Id, entity.Username);
    }
}