﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.Item.Exceptions;

public class ItemNotFoundException : AbstractException
{
    public ItemNotFoundException() : base(message: "Item not found")
    {
    }
}