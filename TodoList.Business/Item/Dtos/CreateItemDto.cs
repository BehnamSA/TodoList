﻿using TodoList.Infrastructure.Persistence.Item;

namespace TodoList.Business.Item.Dtos;

public record CreateItemDto
{
    public int UserStoryId { get; set; }
    public int? UserId { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public int? BusinessValue { get; set; }
    public ItemType Type { get; set; }
    public ItemStatus Status { get; set; }
}