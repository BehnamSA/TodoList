﻿using TodoList.Infrastructure.Persistence.Item;

namespace TodoList.Business.Item.Dtos;

public record UpdateItemDto
{
    public int? UserId { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public int? BusinessValue { get; set; }
    public ItemStatus Status { get; set; }
}