﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.ItemDiscussion.Exceptions;

public class MessageIsRequiredException : AbstractException
{
    public MessageIsRequiredException() : base(message: "Message is required")
    {
    }
}