﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.ItemDiscussion.Exceptions;

public class ItemDiscussionMessageNotFoundException : AbstractException
{
    public ItemDiscussionMessageNotFoundException() : base(message: "Item discussion message not found")
    {
    }
}