﻿using Microsoft.EntityFrameworkCore;
using TodoList.Business.Item.Exceptions;
using TodoList.Business.ItemDiscussion.Exceptions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.Item;
using TodoList.Infrastructure.Persistence.ItemDiscussionMessage;
using TodoList.Infrastructure.Util;

namespace TodoList.Business.ItemDiscussion;

public class ItemDiscussionBusiness
{
    private readonly ProjectManagementDbContext _dbContext;
    private readonly DbSet<ItemDiscussionMessageEntity> _itemDiscussionRepository;
    private readonly DbSet<ItemEntity> _itemRepository;

    public ItemDiscussionBusiness(ProjectManagementDbContext dbContext)
    {
        _dbContext = dbContext;
        _itemDiscussionRepository = _dbContext.Set<ItemDiscussionMessageEntity>();
        _itemRepository = _dbContext.Set<ItemEntity>();
    }

    public async Task<int> CreateAsync(int itemId, int userId, string message)
    {
        Guard.Assert<ItemNotFoundException>(await _itemRepository.AnyAsync(i => i.Id == itemId));

        Guard.Assert<MessageIsRequiredException>(string.IsNullOrWhiteSpace(message) is false);

        var entity = new ItemDiscussionMessageEntity
        {
            ItemId = itemId,
            UserId = userId,
            Message = message,
            CreateDate = DateTime.Now
        };

        _itemDiscussionRepository.Add(entity);

        await _dbContext.SaveChangesAsync();

        return entity.Id;
    }

    public async Task UpdateAsync(int id, string message)
    {
        var entity = await _itemDiscussionRepository.FindAsync(id);

        Guard.Assert<ItemDiscussionMessageNotFoundException>(entity is not null);

        entity.Message = message;

        _itemDiscussionRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id)
    {
        var entity = await _itemDiscussionRepository.FindAsync(id);

        Guard.Assert<ItemDiscussionMessageNotFoundException>(entity is not null);

        _itemDiscussionRepository.Remove(entity);

        await _dbContext.SaveChangesAsync();
    }
}