﻿using Microsoft.EntityFrameworkCore;
using TodoList.Business.Project.ViewModels;
using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.Project;
using TodoList.Infrastructure.Persistence.Project.Exceptions;
using TodoList.Infrastructure.Persistence.ProjectUser;
using TodoList.Infrastructure.Persistence.Sprint;
using TodoList.Infrastructure.Persistence.User;
using TodoList.Infrastructure.Persistence.UserStory;
using TodoList.Infrastructure.Util;

namespace TodoList.Business.Project;

public class ProjectBusiness
{
    private readonly ProjectManagementDbContext _dbContext;
    private DbSet<ProjectEntity> _projectRepository;
    private DbSet<UserStoryEntity> _userStoryRepository;
    private DbSet<SprintEntity> _sprintRepository;
    private DbSet<UserEntity> _userRepository;

    public ProjectBusiness(ProjectManagementDbContext dbContext)
    {
        _dbContext = dbContext;
        _projectRepository = _dbContext.Set<ProjectEntity>();
        _userStoryRepository = _dbContext.Set<UserStoryEntity>();
        _sprintRepository = _dbContext.Set<SprintEntity>();
        _userRepository = _dbContext.Set<UserEntity>();
    }

    public async Task<int> CreateAsync(string title, int userId, string? description = null)
    {
        var entity = new ProjectEntity
        {
            OwnerUserId = userId,
            Title = title,
            Description = description,
        };

        _projectRepository.Add(entity);

        await _dbContext.SaveChangesAsync();

        return entity.Id;
    }

    public async Task<ProjectViewModel[]> GetAsync(int userId)
    {
        var entities = await _projectRepository
            .Include(p => p.ProjectUsers).ThenInclude(pu => pu.User)
            .Include(p => p.Sprints).ThenInclude(s => s.UserStories)
            .ToArrayAsync();

        return entities.Where(p => p.OwnerUserId == userId || p.ProjectUsers.Any(pu => pu.UserId == userId)).Select(
            p => new ProjectViewModel(
                p.Id,
                p.Title,
                p.ProjectUsers.Select(
                    pu => new UserViewModel(pu.User.Id, pu.User.Username)
                ).ToArray(),
                p.Sprints.Select(
                    s => new SprintViewModel
                    {
                        Id = s.Id,
                        ProjectId = s.ProjectId,
                        Start = s.Start.ToStringDateOnly(),
                        End = s.End.ToStringDateOnly(),
                        Title = s.Title,
                        UserStories = s.UserStories.Select(
                            us => new SprintViewModel.UserStoryViewModel(us.Id, us.Title, us.Description)
                        ).ToArray()
                    }
                ).ToArray()
            )
        ).ToArray();
    }

    public async Task<ProjectViewModel> GetByIdAsync(int id)
    {
        var entity = await _projectRepository
            .Include(p => p.ProjectUsers).ThenInclude(pu => pu.User)
            .Include(p => p.Sprints).ThenInclude(s => s.UserStories)
            .SingleOrDefaultAsync(p => p.Id == id);

        Guard.Assert<ProjectNotFoundException>(entity is not null);

        return new ProjectViewModel(
            entity.Id,
            entity.Title,
            entity.ProjectUsers.Select(
                pu => new UserViewModel(pu.User.Id, pu.User.Username)
            ).ToArray(),
            entity.Sprints.Select(
                s => new SprintViewModel
                {
                    Id = s.Id,
                    ProjectId = s.ProjectId,
                    Start = s.Start.ToStringDateOnly(),
                    End = s.End.ToStringDateOnly(),
                    Title = s.Title,
                    UserStories = s.UserStories.Select(
                        us => new SprintViewModel.UserStoryViewModel(us.Id, us.Title, us.Description)
                    ).ToArray()
                }
            ).ToArray()
        );
    }

    public async Task UpdateAsync(int id, string title, string? description = null)
    {
        var entity = await _projectRepository.FindAsync(id);
        Guard.Assert<ProjectNotFoundException>(entity is not null);

        entity!.Title = title;
        entity!.Description = description;

        _projectRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id)
    {
        var entity = await _projectRepository.Include(tl => tl.Sprints)
            .SingleOrDefaultAsync(tl => tl.Id == id);
        Guard.Assert<ProjectNotFoundException>(entity is not null);

        _sprintRepository.RemoveRange(entity!.Sprints);
        _projectRepository.Remove(entity!);

        await _dbContext.SaveChangesAsync();
    }

    public async Task JoinUsersAsync(int projectId, int[] userIds)
    {
        var entity = await _projectRepository.Include(p => p.ProjectUsers)
            .SingleOrDefaultAsync(p => p.Id == projectId);

        Guard.Assert<ProjectNotFoundException>(entity is not null);

        var alreadyJoinedUserIds = entity.ProjectUsers.Select(pu => pu.UserId).ToHashSet();
        var allUserIds = await _userRepository.Select(u => u.Id).ToArrayAsync();

        foreach (var userId in userIds)
        {
            if (alreadyJoinedUserIds.Contains(userId) || allUserIds.Contains(userId) is false)
                continue;

            entity.ProjectUsers.Add(new ProjectUserEntity
            {
                UserId = userId,
                ProjectId = projectId
            });
        }

        await _dbContext.SaveChangesAsync();
    }

    public async Task RemoveUsersAsync(int projectId, int[] userIds)
    {
        var project = await _projectRepository.Include(p => p.ProjectUsers)
            .SingleOrDefaultAsync(p => p.Id == projectId);

        Guard.Assert<ProjectNotFoundException>(project is not null);

        project.ProjectUsers.RemoveAll(pu => userIds.Contains(pu.UserId));

        await _dbContext.SaveChangesAsync();
    }
}

public record TodoItemDto(string Title, string? Description);