﻿namespace TodoList.Business.Project.ViewModels;

public record UserViewModel(int Id, string Username);