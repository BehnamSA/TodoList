﻿namespace TodoList.Business.Project.ViewModels;

public record ProjectViewModel(int Id, string Title, UserViewModel[] Users, SprintViewModel[] Sprints);