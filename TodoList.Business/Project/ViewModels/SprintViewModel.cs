﻿namespace TodoList.Business.Project.ViewModels;

public record SprintViewModel
{
    public int Id { get; set; }
    public int ProjectId { get; set; }
    public string Start { get; set; }
    public string End { get; set; }
    public string Title { get; set; }
    public UserStoryViewModel[] UserStories { get; set; } = Array.Empty<UserStoryViewModel>();
    public int CountOfUserStories => UserStories.Length;

    public record UserStoryViewModel(int Id, string Title, string? Description);
}