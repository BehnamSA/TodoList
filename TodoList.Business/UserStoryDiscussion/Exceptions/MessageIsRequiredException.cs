﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.UserStoryDiscussion.Exceptions;

public class MessageIsRequiredException : AbstractException
{
    public MessageIsRequiredException() : base(message: "Message is required")
    {
    }
}