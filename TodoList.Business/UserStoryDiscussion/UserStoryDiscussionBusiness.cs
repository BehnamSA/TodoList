﻿using Microsoft.EntityFrameworkCore;
using TodoList.Business.UserStory.Exceptions;
using TodoList.Business.UserStoryDiscussion.Exceptions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.UserStory;
using TodoList.Infrastructure.Persistence.UserStoryDiscussionMessage;
using TodoList.Infrastructure.Util;

namespace TodoList.Business.UserStoryDiscussion;

public class UserStoryDiscussionBusiness
{
    private readonly ProjectManagementDbContext _dbContext;
    private readonly DbSet<UserStoryDiscussionMessageEntity> _userStoryDiscussionRepository;
    private readonly DbSet<UserStoryEntity> _userStoryRepository;

    public UserStoryDiscussionBusiness(ProjectManagementDbContext dbContext)
    {
        _dbContext = dbContext;
        _userStoryDiscussionRepository = _dbContext.Set<UserStoryDiscussionMessageEntity>();
        _userStoryRepository = _dbContext.Set<UserStoryEntity>();
    }

    public async Task<int> CreateAsync(int userStoryId, int userId, string message)
    {
        Guard.Assert<UserStoryNotFoundException>(await _userStoryRepository.AnyAsync(us => us.Id == userStoryId));

        Guard.Assert<MessageIsRequiredException>(string.IsNullOrWhiteSpace(message) is false);

        var entity = new UserStoryDiscussionMessageEntity
        {
            UserStoryId = userStoryId,
            UserId = userId,
            Message = message,
            CreateDate = DateTime.Now
        };

        _userStoryDiscussionRepository.Add(entity);

        await _dbContext.SaveChangesAsync();

        return entity.Id;
    }

    public async Task UpdateAsync(int id, string message)
    {
        var entity = await _userStoryDiscussionRepository.FindAsync(id);

        Guard.Assert<UserStoryDiscussionMessageNotFoundException>(entity is not null);

        entity.Message = message;

        _userStoryDiscussionRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id)
    {
        var entity = await _userStoryDiscussionRepository.FindAsync(id);

        Guard.Assert<UserStoryDiscussionMessageNotFoundException>(entity is not null);

        _userStoryDiscussionRepository.Remove(entity);

        await _dbContext.SaveChangesAsync();
    }
}