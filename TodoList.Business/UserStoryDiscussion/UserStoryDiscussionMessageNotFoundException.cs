﻿namespace TodoList.Business.UserStoryDiscussion;

public class UserStoryDiscussionMessageNotFoundException : Exception
{
    public UserStoryDiscussionMessageNotFoundException() : base(message: "User story discussion message not found")
    {
    }
}