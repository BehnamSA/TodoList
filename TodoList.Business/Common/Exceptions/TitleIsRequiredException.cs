﻿namespace TodoList.Business.Common.Exceptions;

public class TitleIsRequiredException : AbstractException
{
    public TitleIsRequiredException() : base(message: "Title is required")
    {
    }
}