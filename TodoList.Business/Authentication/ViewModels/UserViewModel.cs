﻿namespace TodoList.Business.Authentication.ViewModels;

public record UserViewModel(int Id, string Username, string Email, string HashPassword);