﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.Authentication.Exceptions;

public class UsernameNotFoundException : AbstractException
{
    public UsernameNotFoundException() : base(message: "Username not found")
    {
    }
}