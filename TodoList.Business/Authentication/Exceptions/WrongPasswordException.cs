﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.Authentication.Exceptions;

public class WrongPasswordException : AbstractException
{
    public WrongPasswordException() : base(message: "Password is wrong")
    {
    }
}