﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.Authentication.Exceptions;

public class DuplicateUsernameException : AbstractException
{
    public DuplicateUsernameException() : base(message: "Username has already taken")
    {
    }
}