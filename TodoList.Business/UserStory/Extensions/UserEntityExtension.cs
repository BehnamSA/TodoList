﻿using TodoList.Business.UserStory.ViewModels;
using TodoList.Infrastructure.Persistence.User;

namespace TodoList.Business.UserStory.Extensions;

internal static class UserEntityExtension
{
    internal static UserStoryViewModel.UserViewModel? ToViewModel(this UserEntity? entity)
    {
        return entity is null ? null : new UserStoryViewModel.UserViewModel(entity.Id, entity.Username);
    }
}