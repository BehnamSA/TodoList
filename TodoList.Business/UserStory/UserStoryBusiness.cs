﻿using Microsoft.EntityFrameworkCore;
using TodoList.Business.Common.Exceptions;
using TodoList.Business.Sprint.Exceptions;
using TodoList.Business.User.Exceptions;
using TodoList.Business.UserStory.Dtos;
using TodoList.Business.UserStory.Exceptions;
using TodoList.Business.UserStory.Extensions;
using TodoList.Business.UserStory.ViewModels;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.Project;
using TodoList.Infrastructure.Persistence.Sprint;
using TodoList.Infrastructure.Persistence.User;
using TodoList.Infrastructure.Persistence.UserStory;
using TodoList.Infrastructure.Util;

namespace TodoList.Business.UserStory;

public class UserStoryBusiness
{
    private readonly ProjectManagementDbContext _dbContext;
    private readonly DbSet<UserStoryEntity> _userStoryRepository;
    private readonly DbSet<ProjectEntity> _projectRepository;
    private readonly DbSet<UserEntity> _userRepository;
    private readonly DbSet<SprintEntity> _sprintRepository;

    public UserStoryBusiness(ProjectManagementDbContext dbContext)
    {
        _dbContext = dbContext;
        _userStoryRepository = dbContext.Set<UserStoryEntity>();
        _projectRepository = dbContext.Set<ProjectEntity>();
        _userRepository = dbContext.Set<UserEntity>();
        _sprintRepository = dbContext.Set<SprintEntity>();
    }

    public async Task<int> CreateAsync(CreateUserStoryDto dto)
    {
        Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(dto.Title) is false);

        Guard.Assert<SprintNotFoundException>(await _sprintRepository.AnyAsync(p => p.Id == dto.SprintId));

        Guard.Assert<UserNotFoundException>(
            dto.UserId is null || await _userRepository.AnyAsync(u => u.Id == dto.UserId)
        );

        var entity = new UserStoryEntity
        {
            SprintId = dto.SprintId,
            AssignedToUserId = dto.UserId,
            Title = dto.Title,
            Description = dto.Description,
            Status = UserStoryStatus.Pending,
            BusinessValue = dto.BusinessValue
        };

        _userStoryRepository.Add(entity);

        await _dbContext.SaveChangesAsync();

        return entity.Id;
    }

    public async Task<UserStoryViewModel> GetAsync(int id)
    {
        var entity = await _userStoryRepository
            .Include(us => us.AssignedToUser)
            .Include(us => us.Items).ThenInclude(i => i.AssignedToUser)
            .Include(us => us.DiscussionMessages)
            .SingleOrDefaultAsync(us => us.Id == id);

        Guard.Assert<UserStoryNotFoundException>(entity is not null);

        return new UserStoryViewModel
        {
            Id = entity.Id,
            SprintId = entity.SprintId,
            Title = entity.Title,
            Description = entity.Description,
            BusinessValue = entity.BusinessValue,
            Status = entity.Status,
            User = entity.AssignedToUser.ToViewModel(),
            Items = entity.Items.Select(i => new UserStoryViewModel.ItemViewModel
            {
                Id = i.Id,
                UserStoryId = entity.Id,
                Title = i.Title,
                Description = i.Description,
                BusinessValue = i.BusinessValue,
                Status = i.Status,
                Type = i.Type,
                User = i.AssignedToUser.ToViewModel(),
            }).ToArray(),
            Discussions = entity.DiscussionMessages
                .OrderBy(idm => idm.CreateDate)
                .Select(idm => new UserStoryViewModel.DiscussionViewModel(idm.Id, idm.Message, idm.CreateDate))
                .ToArray()
        };
    }

    public async Task UpdateAsync(int id, UpdateUserStoryDto dto)
    {
        var entity = await _userStoryRepository.FindAsync(id);
        Guard.Assert<UserStoryNotFoundException>(entity is not null);

        Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(dto.Title) is false);

        Guard.Assert<UserNotFoundException>(
            dto.UserId is null || await _userRepository.AnyAsync(u => u.Id == dto.UserId)
        );

        entity.AssignedToUserId = dto.UserId;
        entity.Title = dto.Title;
        entity.Description = dto.Description;
        entity.Status = dto.Status;
        entity.BusinessValue = dto.BusinessValue;

        _userStoryRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id)
    {
        var entity = await _userStoryRepository.FindAsync(id);

        if (entity is null) return;

        _userStoryRepository.Remove(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateStatusAsync(int id, UserStoryStatus status)
    {
        var entity = await _userStoryRepository.FindAsync(id);
        Guard.Assert<UserStoryNotFoundException>(entity is not null);

        entity.Status = status;

        _userStoryRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateSprintAsync(int id, int sprintId)
    {
        var entity = await _userStoryRepository.FindAsync(id);
        Guard.Assert<UserStoryNotFoundException>(entity is not null);
        Guard.Assert<SprintNotFoundException>(await _sprintRepository.AnyAsync(p => p.Id == sprintId));

        entity.SprintId = sprintId;

        _userStoryRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }
}