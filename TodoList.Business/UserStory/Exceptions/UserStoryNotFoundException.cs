﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.UserStory.Exceptions;

public class UserStoryNotFoundException : AbstractException
{
    public UserStoryNotFoundException() : base(message: "User story not found")
    {
    }
}