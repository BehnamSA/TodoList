﻿using TodoList.Infrastructure.Persistence.UserStory;

namespace TodoList.Business.UserStory.Dtos;

public record UpdateUserStoryDto
{
    public int? UserId { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public int? BusinessValue { get; set; }
    public UserStoryStatus Status { get; set; }
}