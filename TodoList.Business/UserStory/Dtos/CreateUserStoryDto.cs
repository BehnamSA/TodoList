﻿namespace TodoList.Business.UserStory.Dtos;

public record CreateUserStoryDto
{
    public int SprintId { get; set; }
    public int? UserId { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public int? BusinessValue { get; set; }
}