﻿using Microsoft.Extensions.DependencyInjection;
using TodoList.Business.Authentication;
using TodoList.Business.Item;
using TodoList.Business.ItemDiscussion;
using TodoList.Business.Project;
using TodoList.Business.Sprint;
using TodoList.Business.UserStory;
using TodoList.Business.UserStoryDiscussion;

namespace TodoList.Business;

public class BusinessBootstrapper
{
    public static void Run(IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<ProjectBusiness>();
        serviceCollection.AddScoped<AuthBusiness>();
        serviceCollection.AddScoped<UserStoryBusiness>();
        serviceCollection.AddScoped<UserStoryDiscussionBusiness>();
        serviceCollection.AddScoped<ItemBusiness>();
        serviceCollection.AddScoped<ItemDiscussionBusiness>();
        serviceCollection.AddScoped<SprintBusiness>();
    }
}