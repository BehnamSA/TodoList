﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.User.Exceptions;

public class UserNotFoundException : AbstractException
{
    public UserNotFoundException() : base(message: "User not found")
    {
    }
}