﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.Sprint.Exceptions;

public class SprintNotFoundException : AbstractException
{
    public SprintNotFoundException() : base(message: "Sprint not found")
    {
    }
}