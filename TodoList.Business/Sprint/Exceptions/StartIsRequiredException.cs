﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.Sprint.Exceptions;

public class StartIsRequiredException : AbstractException
{
    public StartIsRequiredException() : base(message: "Start is required")
    {
    }
}