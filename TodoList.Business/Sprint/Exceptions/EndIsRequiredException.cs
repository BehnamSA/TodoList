﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.Sprint.Exceptions;

public class EndIsRequiredException : AbstractException
{
    public EndIsRequiredException() : base(message: "End is required")
    {
    }
}