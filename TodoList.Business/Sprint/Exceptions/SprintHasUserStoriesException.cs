﻿using TodoList.Business.Common.Exceptions;

namespace TodoList.Business.Sprint.Exceptions;

public class SprintHasUserStoriesException : AbstractException
{
    public SprintHasUserStoriesException() : base(message: "Sprint has user stories")
    {
    }
}