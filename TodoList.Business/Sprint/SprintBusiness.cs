﻿using Microsoft.EntityFrameworkCore;
using TodoList.Business.Common.Exceptions;
using TodoList.Business.Sprint.Dtos;
using TodoList.Business.Sprint.Exceptions;
using TodoList.Business.Sprint.ViewModels;
using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.Project;
using TodoList.Infrastructure.Persistence.Project.Exceptions;
using TodoList.Infrastructure.Persistence.Sprint;
using TodoList.Infrastructure.Util;

namespace TodoList.Business.Sprint;

public class SprintBusiness
{
    private readonly ProjectManagementDbContext _dbContext;
    private readonly DbSet<ProjectEntity> _projectRepository;
    private readonly DbSet<SprintEntity> _sprintRepository;

    public SprintBusiness(ProjectManagementDbContext dbContext)
    {
        _dbContext = dbContext;
        _projectRepository = _dbContext.Set<ProjectEntity>();
        _sprintRepository = _dbContext.Set<SprintEntity>();
    }

    public async Task<int> CreateAsync(int projectId, CreateSprintDto dto)
    {
        Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(dto.Title) is false);
        Guard.Assert<StartIsRequiredException>(string.IsNullOrWhiteSpace(dto.Start) is false);
        Guard.Assert<EndIsRequiredException>(string.IsNullOrWhiteSpace(dto.End) is false);

        Guard.Assert<ProjectNotFoundException>(await _projectRepository.AnyAsync(p => p.Id == projectId));

        var entity = new SprintEntity
        {
            ProjectId = projectId,
            Title = dto.Title,
            Start = dto.Start.ToDateTime(),
            End = dto.End.ToDateTime()
        };

        _sprintRepository.Add(entity);

        await _dbContext.SaveChangesAsync();

        return entity.Id;
    }

    public async Task<SprintViewModel?> GetAsync(int id)
    {
        var entity = await _sprintRepository.Include(s => s.UserStories).ThenInclude(us => us.Items)
            .SingleOrDefaultAsync(s => s.Id == id);

        if (entity is null) return null;

        return new SprintViewModel
        {
            Id = entity.Id,
            ProjectId = entity.ProjectId,
            Title = entity.Title,
            Start = entity.Start.ToStringDateOnly(),
            End = entity.End.ToStringDateOnly(),
            UserStories = entity.UserStories.Select(us =>
                new UserStoryViewModel(
                    us.Id, us.Title, us.Description,
                    us.Items.Select(i =>
                        new ItemViewModel
                        {
                            Id = i.Id,
                            UserStoryId = i.UserStoryId,
                            BusinessValue = i.BusinessValue,
                            Title = i.Title,
                            Description = i.Description,
                            Status = i.Status,
                            Type = i.Type,
                            User = i.AssignedToUser is null
                                ? null
                                : new UserViewModel(i.AssignedToUser.Id, i.AssignedToUser.Username),
                        }).ToArray()
                )
            ).ToArray()
        };
    }

    public async Task<SprintViewModel[]> GetByProjectIdAsync(int projectId)
    {
        var entities = await _sprintRepository.Include(s => s.UserStories).ThenInclude(us => us.Items)
            .Where(s => s.ProjectId == projectId).ToArrayAsync();

        return entities.Select(s => new SprintViewModel
            {
                Id = s.Id,
                ProjectId = s.ProjectId,
                Title = s.Title,
                Start = s.Start.ToStringDateOnly(),
                End = s.End.ToStringDateOnly(),
                UserStories = s.UserStories.Select(us =>
                    new UserStoryViewModel(
                        us.Id, us.Title, us.Description,
                        us.Items.Select(i =>
                            new ItemViewModel
                            {
                                Id = i.Id,
                                UserStoryId = i.UserStoryId,
                                BusinessValue = i.BusinessValue,
                                Title = i.Title,
                                Description = i.Description,
                                Status = i.Status,
                                Type = i.Type,
                                User = i.AssignedToUser is null
                                    ? null
                                    : new UserViewModel(i.AssignedToUser.Id, i.AssignedToUser.Username),
                            }).ToArray()
                    )
                ).ToArray()
            }
        ).ToArray();
    }

    public async Task UpdateAsync(int id, UpdateSprintDto dto)
    {
        Guard.Assert<TitleIsRequiredException>(string.IsNullOrWhiteSpace(dto.Title) is false);
        Guard.Assert<StartIsRequiredException>(string.IsNullOrWhiteSpace(dto.Start) is false);
        Guard.Assert<EndIsRequiredException>(string.IsNullOrWhiteSpace(dto.End) is false);

        var entity = await _sprintRepository.FindAsync(id);

        Guard.Assert<SprintNotFoundException>(entity is not null);

        entity.Title = dto.Title;
        entity.Start = dto.Start.ToDateTime();
        entity.End = dto.End.ToDateTime();

        _sprintRepository.Update(entity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id)
    {
        var entity = await _sprintRepository.Include(s => s.UserStories)
            .SingleOrDefaultAsync(s => s.Id == id);

        Guard.Assert<SprintNotFoundException>(entity is not null);
        Guard.Assert<SprintHasUserStoriesException>(entity.UserStories.Any() is false);

        _sprintRepository.Remove(entity);

        await _dbContext.SaveChangesAsync();
    }
}