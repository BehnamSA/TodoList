﻿using TodoList.Infrastructure.Persistence.Item;

namespace TodoList.Business.Sprint.ViewModels;

public record ItemViewModel
{
    public required int Id { get; set; }
    public required int UserStoryId { get; set; }
    public required int? BusinessValue { get; set; }
    public required string Title { get; set; }
    public required string? Description { get; set; }
    public required ItemStatus Status { get; set; }
    public required ItemType Type { get; set; }
    public required UserViewModel? User { get; set; }
}