﻿namespace TodoList.Business.Sprint.ViewModels;

public record UserViewModel(int Id, string Username);