﻿namespace TodoList.Business.Sprint.ViewModels;

public record UserStoryViewModel(int Id, string Title, string? Description, ItemViewModel[] Items)
{
}