﻿namespace TodoList.Business.Sprint.Dtos;

public record UpdateSprintDto
{
    public string Title { get; set; }
    public string Start { get; set; }
    public string End { get; set; }
}