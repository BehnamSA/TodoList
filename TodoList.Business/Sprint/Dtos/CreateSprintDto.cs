﻿namespace TodoList.Business.Sprint.Dtos;

public record CreateSprintDto
{
    public string Title { get; set; }
    public string Start { get; set; }
    public string End { get; set; }
}