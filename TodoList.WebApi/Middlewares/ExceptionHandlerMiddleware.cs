﻿using TodoList.Business.Common.Exceptions;
using TodoList.Infrastructure.Extensions;
using TodoList.WebApi.Controllers;
using TodoList.WebApi.Exceptions;

namespace TodoList.WebApi.Middlewares;

public class ExceptionHandlerMiddleware
{
    private readonly RequestDelegate _next;

    public ExceptionHandlerMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (UnauthenticatedException ex)
        {
            await SetResponseAsync(
                context,
                StatusCodes.Status401Unauthorized,
                ApiResult.SetMessage(ex.Message).SerializeToJson()!
            );
        }
        catch (UnauthorizedException ex)
        {
            await SetResponseAsync(
                context,
                StatusCodes.Status403Forbidden,
                ApiResult.SetMessage(ex.Message).SerializeToJson()!
            );
        }
        catch (ActionNotFoundException)
        {
            SetResponse(context, StatusCodes.Status404NotFound);
        }
        catch (Exception ex)
        {
            if (ex is AbstractException)
            {
                await SetResponseAsync(
                    context,
                    StatusCodes.Status400BadRequest,
                    ApiResult.SetMessage(ex.Message).SerializeToJson()!
                );
            }
            else
            {
                await SetResponseAsync(
                    context,
                    StatusCodes.Status500InternalServerError,
                    ApiResult.SetMessage(ex.Message).SerializeToJson()!
                );
            }
        }
    }

    private async Task SetResponseAsync(HttpContext context, int statusCode, string jsonContent)
    {
        context.Response.StatusCode = statusCode;
        context.Response.ContentType = "application/json";
        await context.Response.WriteAsync(jsonContent);
    }

    private void SetResponse(HttpContext context, int statusCode)
    {
        context.Response.StatusCode = statusCode;
    }
}