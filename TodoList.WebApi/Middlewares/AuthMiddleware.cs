﻿using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.User;
using TodoList.Infrastructure.Util;
using TodoList.WebApi.Controllers;
using TodoList.WebApi.Controllers.Auth;
using TodoList.WebApi.Exceptions;

namespace TodoList.WebApi.Middlewares;

public class AuthMiddleware
{
    private readonly RequestDelegate _next;

    public AuthMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        context.Request.RouteValues.TryGetValue("action", out var objActionName);

        var actionName = objActionName?.ToString();

        Guard.Assert<ActionNotFoundException>(string.IsNullOrWhiteSpace(actionName) is false);

        if (actionName != nameof(AuthController.Login) && actionName != nameof(AuthController.Register))
        {
            var token = context.Request.Headers.Authorization.ToString();
            Guard.Assert<UnauthenticatedException>(string.IsNullOrWhiteSpace(token) is false);
            Guard.Assert<UnauthenticatedException>(token.IsBase64());

            var tokenModel = TokenModel.Parse(token.DecodeBase64()!);

            var dbContext = context.RequestServices.GetRequiredService<ProjectManagementDbContext>();
            var user = dbContext.Set<UserEntity>().Find(tokenModel.UserId);
            Guard.Assert<UnauthenticatedException>(user is not null);
        }

        await _next(context);
    }
}