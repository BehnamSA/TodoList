﻿namespace TodoList.WebApi.Exceptions;

public class UnauthenticatedException : Exception
{
    public UnauthenticatedException() : base(message: "User must login")
    {
    }
}