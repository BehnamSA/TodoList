﻿namespace TodoList.WebApi.Exceptions;

public class UnauthorizedException : Exception
{
    public UnauthorizedException() : base(message: "User access denied")
    {
    }
}