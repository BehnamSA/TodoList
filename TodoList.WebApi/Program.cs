using TodoList.Business;
using TodoList.Infrastructure.Persistence;
using TodoList.WebApi.Middlewares;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(corsBuilder => corsBuilder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
});

DataAccessBootstrapper.Run(builder.Services);
BusinessBootstrapper.Run(builder.Services);

var app = builder.Build();

EnsureDatabaseIsCreated(app);

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.UseCors();

app.UseMiddleware<ExceptionHandlerMiddleware>();
app.UseMiddleware<AuthMiddleware>();

app.MapControllers();

app.Run();




static void EnsureDatabaseIsCreated(WebApplication app)
{
    var scope = app.Services.CreateScope();
    var dbContext = scope.ServiceProvider.GetRequiredService<ProjectManagementDbContext>();
    dbContext.Database.EnsureCreated();
}