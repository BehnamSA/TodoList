﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Business.Item;
using TodoList.Business.Item.Dtos;
using TodoList.Business.ItemDiscussion;
using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence.Item;

namespace TodoList.WebApi.Controllers.Item;

[AuthorizeBePartOfProject]
public class ItemController : AbstractController
{
    private readonly ItemBusiness _itemBusiness;
    private readonly ItemDiscussionBusiness _itemDiscussionBusiness;

    public ItemController(ItemBusiness itemBusiness, ItemDiscussionBusiness itemDiscussionBusiness)
    {
        _itemBusiness = itemBusiness;
        _itemDiscussionBusiness = itemDiscussionBusiness;
    }

    [HttpPost]
    public async Task<IActionResult> Post([FromQuery] int userStoryId, [FromBody] CreateItemDto dto)
    {
        dto.UserStoryId = userStoryId;
        var id = await _itemBusiness.CreateAsync(dto);
        return Response(id);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        var viewModel = await _itemBusiness.GetAsync(id);
        return Response(viewModel);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(int id, UpdateItemDto dto)
    {
        await _itemBusiness.UpdateAsync(id, dto);
        return Ok();
    }

    [HttpPatch("{id}/{status}")]
    public async Task<IActionResult> Patch(int id, int status)
    {
        await _itemBusiness.UpdateStatusAsync(id, status.ToEnum<ItemStatus>());
        return Ok();
    }

    [HttpPost("{id}/message")]
    public async Task<IActionResult> AddMessage(int id, [FromBody] ItemDiscussionMessageDto dto)
    {
        var itemDiscussionId = await _itemDiscussionBusiness.CreateAsync(id, GetToken().UserId, dto.Message);
        return Response(itemDiscussionId);
    }

    [HttpPut("{id}/message/{messageId}")]
    public async Task<IActionResult> UpdateMessage(int id, int messageId, [FromBody] ItemDiscussionMessageDto dto)
    {
        await _itemDiscussionBusiness.UpdateAsync(messageId, dto.Message);
        return Ok();
    }

    [HttpDelete("{id}/message/{messageId}")]
    public async Task<IActionResult> DeleteMessage(int id, int messageId)
    {
        await _itemDiscussionBusiness.DeleteAsync(messageId);
        return Ok();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _itemBusiness.DeleteAsync(id);
        return Ok();
    }
}