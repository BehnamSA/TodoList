﻿using Microsoft.AspNetCore.Mvc.Filters;
using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.Item;
using TodoList.Infrastructure.Persistence.Sprint;
using TodoList.Infrastructure.Persistence.UserStory;
using TodoList.Infrastructure.Util;
using TodoList.WebApi.Exceptions;

namespace TodoList.WebApi.Controllers.Item;

public class AuthorizeBePartOfProjectAttribute : AbstractAuthorizeAttribute
{
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        var tokenModel = GetTokenModel(context.HttpContext);

        var dbContext = GetDbContext(context.HttpContext);

        var projectId = GetProjectId(context.HttpContext, dbContext);

        if (projectId is not null)
        {
            var project = dbContext.Projects.Find(projectId)!;

            var partOfProjectUserIds = dbContext.ProjectUsers
                .Where(pu => pu.ProjectId == projectId)
                .Select(pu => pu.UserId)
                .ToArray();

            Guard.Assert<UnauthorizedException>(
                project.OwnerUserId == tokenModel.UserId
                || partOfProjectUserIds.Contains(tokenModel.UserId)
            );
        }

        base.OnActionExecuting(context);
    }


    private int? GetProjectId(HttpContext httpContext, ProjectManagementDbContext dbContext)
    {
        var itemId = httpContext.Request.RouteValues["id"]?.ToString()?.ToInt() ?? null;

        int? userStoryId = null;

        if (itemId is null)
        {
            var queryString = httpContext.Request.QueryString.ToString();
            var userStoryIdFromQueryString = queryString.ToLower().Replace("?userstoryid=", string.Empty).TrimEnd('/');

            if (string.IsNullOrWhiteSpace(userStoryIdFromQueryString) is false
                && int.TryParse(userStoryIdFromQueryString, out _))
            {
                userStoryId = userStoryIdFromQueryString.ToInt();
            }
        }
        else
        {
            userStoryId = dbContext.Set<ItemEntity>().Find(itemId)?.UserStoryId;
        }

        if (userStoryId is null) return null;

        var userStory = dbContext.Set<UserStoryEntity>().Find(userStoryId.Value);
        if (userStory is null) return null;

        return dbContext.Set<SprintEntity>().Find(userStory.SprintId)!.ProjectId;
    }
}