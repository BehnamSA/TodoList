﻿namespace TodoList.WebApi.Controllers.Item;

public record ItemDiscussionMessageDto
{
    public string Message { get; set; }
}