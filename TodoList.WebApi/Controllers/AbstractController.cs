﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Infrastructure.Extensions;

namespace TodoList.WebApi.Controllers;

[ApiController]
[Route("/api/[controller]")]
public abstract class AbstractController : ControllerBase
{
    protected TokenModel GetToken()
    {
        var token = HttpContext.Request.Headers.Authorization.ToString();

        return TokenModel.Parse(token.DecodeBase64()!);
    }

    protected new IActionResult Response<T>(T? value)
    {
        if (value is null) return NoContent();

        return Ok(ApiResult.SetData(value));
    }
}