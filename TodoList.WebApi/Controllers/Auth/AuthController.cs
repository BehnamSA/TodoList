﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoList.Business.Authentication;
using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.User;
using TodoList.WebApi.Controllers.User;

namespace TodoList.WebApi.Controllers.Auth;

public class AuthController : AbstractController
{
    private readonly AuthBusiness _authBusiness;
    private readonly DbSet<UserEntity> _usersRepository;

    public AuthController(AuthBusiness authBusiness, ProjectManagementDbContext dbContext)
    {
        _authBusiness = authBusiness;
        _usersRepository = dbContext.Set<UserEntity>();
    }

    [HttpGet("/api/Login")]
    public async Task<IActionResult> Login([FromQuery] string username, [FromQuery] string password)
    {
        await _authBusiness.LoginAsync(username, password);

        var user = (await _usersRepository.SingleOrDefaultAsync(u => u.Username == username))!;

        var token = new TokenModel
        {
            UserId = user.Id,
            Username = user.Username
        }.ToBase64()!;

        return Response(token);
    }

    [HttpPost("/api/Register")]
    public async Task<IActionResult> Register([FromBody] AuthBusiness.RegisterDto dto)
    {
        await _authBusiness.RegisterAsync(dto);
        return Ok();
    }

    [HttpGet("/api/profile")]
    public async Task<IActionResult> GetProfile()
    {
        var token = GetToken();

        var entity = (await _usersRepository.FindAsync(token.UserId))!;

        return Ok(new UserViewModel(entity.Id, entity.Username, entity.Email));
    }
}