﻿using TodoList.Infrastructure.Extensions;

namespace TodoList.WebApi.Controllers;

public class TokenModel
{
    public required int UserId { get; set; }
    public required string Username { get; set; }

    public override string ToString() => $"{UserId},{Username}";

    public static TokenModel Parse(string csvToken)
    {
        var values = csvToken.Split(",");

        return new TokenModel
        {
            UserId = values[0].ToInt(),
            Username = values[1],
        };
    }
}