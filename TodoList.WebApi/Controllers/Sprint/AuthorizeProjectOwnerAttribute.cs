﻿using Microsoft.AspNetCore.Mvc.Filters;
using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Util;
using TodoList.WebApi.Exceptions;

namespace TodoList.WebApi.Controllers.Sprint;

public class AuthorizeProjectOwnerAttribute : AbstractAuthorizeAttribute
{
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        var tokenModel = GetTokenModel(context.HttpContext);

        var queryString = context.HttpContext.Request.QueryString.ToString();
        var projectIdFromQueryString = queryString.ToLower().Replace("?projectid=", string.Empty).TrimEnd('/');
        
        if (string.IsNullOrWhiteSpace(projectIdFromQueryString) is false)
        {
            var dbContext = context.HttpContext.RequestServices.GetRequiredService<ProjectManagementDbContext>();
            var project = dbContext.Projects.Find(projectIdFromQueryString!.ToInt())!;
            Guard.Assert<UnauthorizedException>(project.OwnerUserId == tokenModel.UserId);
        }

        base.OnActionExecuting(context);
    }
}