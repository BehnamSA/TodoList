﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Business.Sprint;
using TodoList.Business.Sprint.Dtos;

namespace TodoList.WebApi.Controllers.Sprint;

public class SprintController : AbstractController
{
    private readonly SprintBusiness _sprintBusiness;

    public SprintController(SprintBusiness sprintBusiness)
    {
        _sprintBusiness = sprintBusiness;
    }

    [HttpPost]
    [AuthorizeProjectOwner]
    public async Task<IActionResult> Post([FromQuery] int projectId, [FromBody] CreateSprintDto dto)
    {
        var id = await _sprintBusiness.CreateAsync(projectId, dto);
        return Response(id);
    }

    [HttpGet("{id}")]
    [AuthorizeBePartOfProject]
    public async Task<IActionResult> Get(int id)
    {
        var viewModel = await _sprintBusiness.GetAsync(id);
        return Response(viewModel);
    }

    [HttpGet]
    [AuthorizeBePartOfProject]
    public async Task<IActionResult> GetByProjectId([FromQuery] int projectId)
    {
        var viewModels = await _sprintBusiness.GetByProjectIdAsync(projectId);
        return Response(viewModels);
    }

    [HttpPut("{id}")]
    [AuthorizeProjectOwner]
    public async Task<IActionResult> Put(int id, [FromBody] UpdateSprintDto dto)
    {
        await _sprintBusiness.UpdateAsync(id, dto);
        return Ok();
    }

    [HttpDelete("{id}")]
    [AuthorizeProjectOwner]
    public async Task<IActionResult> Delete(int id)
    {
        await _sprintBusiness.DeleteAsync(id);
        return Ok();
    }
}