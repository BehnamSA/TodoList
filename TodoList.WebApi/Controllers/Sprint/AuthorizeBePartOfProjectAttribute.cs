﻿using Microsoft.AspNetCore.Mvc.Filters;
using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.Sprint;
using TodoList.Infrastructure.Util;
using TodoList.WebApi.Exceptions;

namespace TodoList.WebApi.Controllers.Sprint;

public class AuthorizeBePartOfProjectAttribute : AbstractAuthorizeAttribute
{
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        var tokenModel = GetTokenModel(context.HttpContext);

        var dbContext = GetDbContext(context.HttpContext);

        var projectId = GetProjectId(context.HttpContext, dbContext);

        if (projectId is not null)
        {
            var project = dbContext.Projects.Find(projectId);

            var partOfProjectUserIds = dbContext.ProjectUsers
                .Where(pu => pu.ProjectId == projectId)
                .Select(pu => pu.UserId)
                .ToArray();

            Guard.Assert<UnauthorizedException>(
                project?.OwnerUserId == tokenModel.UserId
                || partOfProjectUserIds.Contains(tokenModel.UserId)
            );
        }

        base.OnActionExecuting(context);
    }

    private int? GetProjectId(HttpContext httpContext, ProjectManagementDbContext dbContext)
    {
        var sprintId = httpContext.Request.RouteValues["id"]?.ToString()?.ToInt() ?? null;

        int? projectId = null;

        if (sprintId is null)
        {
            var queryString = httpContext.Request.QueryString.ToString();
            var projectIdFromQueryString = queryString.ToLower().Replace("?projectid=", string.Empty).TrimEnd('/');

            if (string.IsNullOrWhiteSpace(projectIdFromQueryString) is false
                && int.TryParse(projectIdFromQueryString, out _))
            {
                projectId = projectIdFromQueryString.ToInt();
            }
        }
        else
        {
            projectId = dbContext.Set<SprintEntity>().Find(sprintId)?.ProjectId;
        }

        return projectId;
    }
}