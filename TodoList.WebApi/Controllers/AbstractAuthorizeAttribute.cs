﻿using Microsoft.AspNetCore.Mvc.Filters;
using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence;

namespace TodoList.WebApi.Controllers;

public class AbstractAuthorizeAttribute : ActionFilterAttribute
{
    protected TokenModel GetTokenModel(HttpContext httpContext)
    {
        return TokenModel.Parse(
            httpContext.Request.Headers.Authorization.ToString().DecodeBase64()!
        );
    }

    protected ProjectManagementDbContext GetDbContext(HttpContext httpContext)
    {
        return httpContext.RequestServices.GetRequiredService<ProjectManagementDbContext>();
    }
} 