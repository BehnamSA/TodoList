﻿namespace TodoList.WebApi.Controllers;

public record ApiResult
{
    public object? Data { get; set; }
    public string? Message { get; set; }

    public static ApiResult SetData<T>(T data)
    {
        return new ApiResult
        {
            Data = data
        };
    }

    public static ApiResult SetMessage(string message)
    {
        return new ApiResult
        {
            Message = message
        };
    }
}