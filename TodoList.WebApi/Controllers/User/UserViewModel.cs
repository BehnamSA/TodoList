﻿namespace TodoList.WebApi.Controllers.User;

public record UserViewModel(int Id, string Username, string Email);