﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Persistence.User;

namespace TodoList.WebApi.Controllers.User;

public class UserController : AbstractController
{
    private readonly DbSet<UserEntity> _userRepository;

    public UserController(ProjectManagementDbContext dbContext)
    {
        _userRepository = dbContext.Set<UserEntity>();
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
        var users = await _userRepository.ToArrayAsync();

        return Response(
            users.Select(u => new UserViewModel(u.Id, u.Username, u.Email)).ToArray()
        );
    }
}