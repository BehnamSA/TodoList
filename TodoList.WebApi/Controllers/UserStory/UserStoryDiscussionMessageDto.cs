﻿namespace TodoList.WebApi.Controllers.UserStory;

public record UserStoryDiscussionMessageDto
{
    public string Message { get; set; }
}