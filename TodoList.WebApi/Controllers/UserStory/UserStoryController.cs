﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Business.UserStory;
using TodoList.Business.UserStory.Dtos;
using TodoList.Business.UserStoryDiscussion;
using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence.UserStory;

namespace TodoList.WebApi.Controllers.UserStory;

[AuthorizeBePartOfProject]
public class UserStoryController : AbstractController
{
    private readonly UserStoryBusiness _userStoryBusiness;
    private readonly UserStoryDiscussionBusiness _userStoryDiscussionBusiness;

    public UserStoryController(UserStoryBusiness userStoryBusiness,
        UserStoryDiscussionBusiness userStoryDiscussionBusiness)
    {
        _userStoryBusiness = userStoryBusiness;
        _userStoryDiscussionBusiness = userStoryDiscussionBusiness;
    }

    [HttpPost]
    public async Task<IActionResult> Post([FromQuery] int sprintId, [FromBody] CreateUserStoryDto dto)
    {
        dto.SprintId = sprintId;
        var id = await _userStoryBusiness.CreateAsync(dto);
        return Response(id);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        var viewModel = await _userStoryBusiness.GetAsync(id);
        return Response(viewModel);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(int id, UpdateUserStoryDto dto)
    {
        await _userStoryBusiness.UpdateAsync(id, dto);
        return Ok();
    }

    [HttpPatch("{id}")]
    public async Task<IActionResult> PatchSprint(int id, [FromQuery] int sprintId)
    {
        await _userStoryBusiness.UpdateSprintAsync(id, sprintId);
        return Ok();
    }

    [HttpPatch("{id}/{status}")]
    public async Task<IActionResult> Patch(int id, int status)
    {
        await _userStoryBusiness.UpdateStatusAsync(id, status.ToEnum<UserStoryStatus>());
        return Ok();
    }

    [HttpPost("{id}/message")]
    public async Task<IActionResult> AddMessage(int id, [FromBody] UserStoryDiscussionMessageDto dto)
    {
        var itemDiscussionId = await _userStoryDiscussionBusiness.CreateAsync(id, GetToken().UserId, dto.Message);
        return Response(itemDiscussionId);
    }

    [HttpPut("{id}/message/{messageId}")]
    public async Task<IActionResult> UpdateMessage(int id, int messageId, [FromBody] UserStoryDiscussionMessageDto dto)
    {
        await _userStoryDiscussionBusiness.UpdateAsync(messageId, dto.Message);
        return Ok();
    }

    [HttpDelete("{id}/message/{messageId}")]
    public async Task<IActionResult> DeleteMessage(int id, int messageId)
    {
        await _userStoryDiscussionBusiness.DeleteAsync(messageId);
        return Ok();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _userStoryBusiness.DeleteAsync(id);
        return Ok();
    }
}