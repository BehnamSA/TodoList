﻿namespace TodoList.WebApi.Controllers.Project;

public record ProjectDto
{
    public string Title { get; set; }
    public string? Description { get; set; }
}