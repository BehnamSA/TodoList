﻿using Microsoft.AspNetCore.Mvc;
using TodoList.Business.Project;

namespace TodoList.WebApi.Controllers.Project;

public class ProjectController : AbstractController
{
    private readonly ProjectBusiness _projectBusiness;

    public ProjectController(ProjectBusiness projectBusiness)
    {
        _projectBusiness = projectBusiness;
    }

    [HttpPost]
    public async Task<IActionResult> Post(ProjectDto dto)
    {
        var id = await _projectBusiness.CreateAsync(dto.Title, GetToken().UserId, dto.Description);

        return Response(id);
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
        var projects = await _projectBusiness.GetAsync(GetToken().UserId);

        return Response(projects);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        var projects = await _projectBusiness.GetByIdAsync(id);

        return Response(projects);
    }

    [HttpPut("{id}")]
    [AuthorizeProjectOwner]
    public async Task<IActionResult> Put(int id, [FromBody] ProjectDto dto)
    {
        await _projectBusiness.UpdateAsync(id, dto.Title, dto.Description);

        return Ok();
    }

    [HttpDelete("{id}")]
    [AuthorizeProjectOwner]
    public async Task<IActionResult> Delete(int id)
    {
        await _projectBusiness.DeleteAsync(id);

        return Ok();
    }

    [HttpPatch("{id}/JoinUser")]
    [AuthorizeProjectOwner]
    public async Task<IActionResult> JoinUser(int id, [FromQuery] int[] userIds)
    {
        await _projectBusiness.JoinUsersAsync(id, userIds);
        return Ok();
    }

    [HttpPatch("{id}/RemoveUser")]
    [AuthorizeProjectOwner]
    public async Task<IActionResult> RemoveUser(int id, [FromQuery] int[] userIds)
    {
        await _projectBusiness.RemoveUsersAsync(id, userIds);
        return Ok();
    }
}