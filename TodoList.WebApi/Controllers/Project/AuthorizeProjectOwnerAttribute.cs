﻿using Microsoft.AspNetCore.Mvc.Filters;
using TodoList.Infrastructure.Extensions;
using TodoList.Infrastructure.Persistence;
using TodoList.Infrastructure.Util;
using TodoList.WebApi.Exceptions;

namespace TodoList.WebApi.Controllers.Project;

public class AuthorizeProjectOwnerAttribute : AbstractAuthorizeAttribute
{
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        var tokenModel = GetTokenModel(context.HttpContext);

        context.HttpContext.Request.RouteValues.TryGetValue("id", out var objProjectId);

        var projectId = objProjectId?.ToString();

        if (string.IsNullOrWhiteSpace(projectId) is false)
        {
            var dbContext = context.HttpContext.RequestServices.GetRequiredService<ProjectManagementDbContext>();
            var project = dbContext.Projects.Find(projectId!.ToInt())!;
            Guard.Assert<UnauthorizedException>(project.OwnerUserId == tokenModel.UserId);
        }

        base.OnActionExecuting(context);
    }
}