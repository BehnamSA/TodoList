﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace TodoList.Infrastructure.Persistence;

public class DataAccessBootstrapper
{
    public static void Run(IServiceCollection serviceCollection)
    {
        serviceCollection.AddDbContext<ProjectManagementDbContext>(
            options => options.UseInMemoryDatabase($"TodoDb-{Guid.NewGuid()}"), ServiceLifetime.Singleton
        );
    }
}