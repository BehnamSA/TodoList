﻿using TodoList.Infrastructure.Persistence.Project;
using TodoList.Infrastructure.Persistence.UserStory;

namespace TodoList.Infrastructure.Persistence.Sprint;

public class SprintEntity
{
    public int Id { get; set; }
    public int ProjectId { get; set; }
    public DateTime Start { get; set; }
    public DateTime End { get; set; }
    public string Title { get; set; }

    public ProjectEntity Project { get; set; }
    public List<UserStoryEntity> UserStories { get; set; }
}