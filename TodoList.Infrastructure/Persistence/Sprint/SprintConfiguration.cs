﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.Infrastructure.Persistence.Sprint;

public class SprintConfiguration : IEntityTypeConfiguration<SprintEntity>
{
    public void Configure(EntityTypeBuilder<SprintEntity> builder)
    {
        builder.HasKey(s => s.Id);
        builder.HasOne(s => s.Project).WithMany(p => p.Sprints).HasForeignKey(s => s.ProjectId).IsRequired();
        builder.Property(s => s.Title).HasMaxLength(256).IsRequired();
    }
}