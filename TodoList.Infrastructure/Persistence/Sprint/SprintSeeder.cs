﻿using Microsoft.EntityFrameworkCore;

namespace TodoList.Infrastructure.Persistence.Sprint;

public class SprintSeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<SprintEntity>().HasData(new[]
        {
            new SprintEntity
            {
                Id = 1,
                ProjectId = 1,
                Title = "Sprint - 1",
                Start = DateTime.Now.AddDays(-14),
                End = DateTime.Now.AddDays(-1)
            },
            new SprintEntity
            {
                Id = 2,
                ProjectId = 1,
                Title = "Sprint - 2",
                Start = DateTime.Now,
                End = DateTime.Now.AddDays(14)
            }
        });
    }
}