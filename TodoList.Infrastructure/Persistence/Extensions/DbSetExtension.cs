﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace TodoList.Infrastructure.Persistence.Extensions;

public static class DbSetExtension
{
    public static void RemoveAll<TEntity>(this DbSet<TEntity> dbSet, Expression<Func<TEntity, bool>> predicate)
        where TEntity : class, new()
    {
        var entities = dbSet.Where(predicate).ToArray();
        dbSet.RemoveRange(entities);
    }
}