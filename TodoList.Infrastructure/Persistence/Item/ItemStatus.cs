﻿namespace TodoList.Infrastructure.Persistence.Item;

public enum ItemStatus
{
    Pending = 1,
    InProgress,
    Review,
    Done
}