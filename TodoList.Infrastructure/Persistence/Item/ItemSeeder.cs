﻿using Microsoft.EntityFrameworkCore;

namespace TodoList.Infrastructure.Persistence.Item;

public class ItemSeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ItemEntity>().HasData(new[]
        {
            new ItemEntity
            {
                Id = 1,
                UserStoryId = 1,
                Title = "Implement the RestAPI of the register user",
                BusinessValue = 10,
                Description =
                    "Implement the register user RestAPI with Username, Email, Password and Phone Number fields",
                Type = ItemType.Task,
                Status = ItemStatus.Done,
                AssignedToUserId = 2
            },
            new ItemEntity
            {
                Id = 2,
                UserStoryId = 1,
                Title = "Design and implement register form",
                BusinessValue = 11,
                Description = "Design the register form using material UI and implement the form using React.Js",
                Type = ItemType.Task,
                Status = ItemStatus.InProgress,
                AssignedToUserId = 2
            },
            new ItemEntity
            {
                Id = 3,
                UserStoryId = 2,
                Title = "Error on whitespace username",
                Description =
                    "When user tries to login using a whitespace username, the Null Reference Exception shows up",
                Type = ItemType.Bug,
                Status = ItemStatus.Pending,
                AssignedToUserId = 2
            }
        });
    }
}