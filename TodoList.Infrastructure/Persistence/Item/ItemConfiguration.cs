﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.Infrastructure.Persistence.Item;

public class ItemConfiguration : IEntityTypeConfiguration<ItemEntity>
{
    public void Configure(EntityTypeBuilder<ItemEntity> builder)
    {
        builder.HasKey(i => i.Id);
        builder.HasOne(i => i.UserStory).WithMany(us => us.Items).HasForeignKey(i => i.UserStoryId).IsRequired();
        builder.HasOne(i => i.AssignedToUser).WithMany(us => us.Items).HasForeignKey(i => i.AssignedToUserId)
            .IsRequired(false);
        builder.Property(i => i.Title).HasMaxLength(256).IsRequired();
        builder.Property(i => i.Description).HasMaxLength(1024);
        builder.Property(i => i.Status).HasConversion<int>();
        builder.Property(i => i.BusinessValue).IsRequired(false);
    }
}