﻿using TodoList.Infrastructure.Persistence.ItemDiscussionMessage;
using TodoList.Infrastructure.Persistence.User;
using TodoList.Infrastructure.Persistence.UserStory;

namespace TodoList.Infrastructure.Persistence.Item;

public class ItemEntity
{
    public int Id { get; set; }
    public int UserStoryId { get; set; }
    public int? AssignedToUserId { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public ItemStatus Status { get; set; }
    public ItemType Type { get; set; }
    public int? BusinessValue { get; set; }

    public UserStoryEntity? UserStory { get; set; }
    public UserEntity? AssignedToUser { get; set; }
    public List<ItemDiscussionMessageEntity> DiscussionMessages { get; set; } = new();
}