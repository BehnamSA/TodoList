﻿namespace TodoList.Infrastructure.Persistence.Item;

public enum ItemType
{
    Task = 1,
    Bug
}