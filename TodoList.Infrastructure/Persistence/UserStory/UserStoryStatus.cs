﻿namespace TodoList.Infrastructure.Persistence.UserStory;

public enum UserStoryStatus
{
    Pending = 1,
    InProgress,
    Review,
    Done
}