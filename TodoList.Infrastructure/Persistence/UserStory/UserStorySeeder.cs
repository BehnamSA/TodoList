﻿using Microsoft.EntityFrameworkCore;

namespace TodoList.Infrastructure.Persistence.UserStory;

public class UserStorySeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<UserStoryEntity>().HasData(new[]
        {
            new UserStoryEntity
            {
                Id = 1,
                SprintId = 1,
                Title = "Register",
                Description = "User can register him self, using his unique username, password, email and phone number",
                Status = UserStoryStatus.Pending
            },
            new UserStoryEntity
            {
                Id = 2,
                SprintId = 1,
                Title = "Login",
                Description =
                    "An already registered user should be able to login into the system using his username and password",
                Status = UserStoryStatus.Pending
            },
            new UserStoryEntity
            {
                Id = 3,
                SprintId = 1,
                Title = "Group Ledgers",
                Description = "The user can create, update and delete the Group Ledgers",
                Status = UserStoryStatus.Pending
            },
            new UserStoryEntity
            {
                Id = 4,
                SprintId = 2,
                Title = "Group Ledgers",
                Description = "The user can create, update and delete the Group Ledgers",
                Status = UserStoryStatus.Pending
            },
        });
    }
}