﻿using TodoList.Infrastructure.Persistence.Item;
using TodoList.Infrastructure.Persistence.Project;
using TodoList.Infrastructure.Persistence.Sprint;
using TodoList.Infrastructure.Persistence.User;
using TodoList.Infrastructure.Persistence.UserStoryDiscussionMessage;

namespace TodoList.Infrastructure.Persistence.UserStory;

public class UserStoryEntity
{
    public int Id { get; set; }
    public int SprintId { get; set; }
    public int? AssignedToUserId { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public bool IsDone => Status == UserStoryStatus.Done;
    public UserStoryStatus Status { get; set; }
    public int? BusinessValue { get; set; }

    public SprintEntity Sprint { get; set; }
    public UserEntity? AssignedToUser { get; set; }

    public List<ItemEntity> Items { get; set; } = new();
    public List<UserStoryDiscussionMessageEntity> DiscussionMessages { get; set; } = new();
}