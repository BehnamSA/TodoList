﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.Infrastructure.Persistence.UserStory;

public class UserStoryConfiguration : IEntityTypeConfiguration<UserStoryEntity>
{
    public void Configure(EntityTypeBuilder<UserStoryEntity> builder)
    {
        builder.HasKey(us => us.Id);
        builder.HasOne(us => us.Sprint).WithMany(s => s.UserStories).HasForeignKey(us => us.SprintId);
        builder.HasOne(us => us.AssignedToUser).WithMany(u => u.UserStories).HasForeignKey(us => us.AssignedToUserId)
            .IsRequired(false);
        builder.Property(us => us.Title).HasMaxLength(256).IsRequired();
        builder.Property(us => us.Description).HasMaxLength(1024);
        builder.Property(us => us.Status).HasConversion<int>();
        builder.Property(us => us.BusinessValue).IsRequired(false);
    }
}