﻿using TodoList.Infrastructure.Persistence.Item;
using TodoList.Infrastructure.Persistence.ItemDiscussionMessage;
using TodoList.Infrastructure.Persistence.Project;
using TodoList.Infrastructure.Persistence.ProjectUser;
using TodoList.Infrastructure.Persistence.UserStory;
using TodoList.Infrastructure.Persistence.UserStoryDiscussionMessage;

namespace TodoList.Infrastructure.Persistence.User;

public class UserEntity
{
    public int Id { get; set; }
    public string Username { get; set; }
    public string HashPassword { get; set; }
    public string Email { get; set; }

    public List<ProjectEntity> OwnedProjects { get; set; }
    public List<ProjectUserEntity> ProjectUsers { get; set; } = new();
    public List<UserStoryEntity> UserStories { get; set; } = new();
    public List<ItemEntity> Items { get; set; } = new();
    public List<ItemDiscussionMessageEntity> ItemDiscussionMessages { get; set; } = new();
    public List<UserStoryDiscussionMessageEntity> UserStoryDiscussionMessages { get; set; } = new();
}