﻿using Microsoft.EntityFrameworkCore;
using TodoList.Infrastructure.Extensions;

namespace TodoList.Infrastructure.Persistence.User;

public class UserSeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<UserEntity>().HasData(new[]
        {
            new UserEntity
            {
                Id = 1,
                Username = "admin",
                HashPassword = "admin".ToBase64()!,
                Email = "admin@email.com"
            },
            new UserEntity
            {
                Id = 2,
                Username = "aida",
                HashPassword = "aida".ToBase64()!,
                Email = "aida@email.com"
            },
            new UserEntity
            {
                Id = 3,
                Username = "guest",
                HashPassword = "guest".ToBase64()!,
                Email = "guest@email.com"
            },
        });
    }
}