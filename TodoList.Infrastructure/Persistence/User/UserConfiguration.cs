﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.Infrastructure.Persistence.User;

public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
{
    public void Configure(EntityTypeBuilder<UserEntity> builder)
    {
        builder.HasKey(u => u.Id);
        builder.Property(u => u.Username).HasMaxLength(128).IsRequired();
        builder.Property(u => u.HashPassword).HasMaxLength(1024).IsRequired();
        builder.Property(u => u.Email).HasMaxLength(256).IsRequired();
    }
}