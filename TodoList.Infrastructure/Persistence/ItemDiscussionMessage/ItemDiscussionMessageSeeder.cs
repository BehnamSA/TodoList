﻿using Microsoft.EntityFrameworkCore;

namespace TodoList.Infrastructure.Persistence.ItemDiscussionMessage;

public class ItemDiscussionMessageSeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ItemDiscussionMessageEntity>().HasData(new[]
        {
            new ItemDiscussionMessageEntity
            {
                Id = 1,
                ItemId = 3,
                UserId = 2,
                Message = "I checked the bug and I saw nothing",
                CreateDate = DateTime.Now.AddHours(-1)
            },
            new ItemDiscussionMessageEntity
            {
                Id = 2,
                ItemId = 3,
                UserId = 1,
                Message = "Please check the bug on version 0.0.1",
                CreateDate = DateTime.Now
            },
        });
    }
}