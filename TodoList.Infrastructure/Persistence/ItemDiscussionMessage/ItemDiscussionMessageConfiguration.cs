﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.Infrastructure.Persistence.ItemDiscussionMessage;

public class ItemDiscussionMessageConfiguration : IEntityTypeConfiguration<ItemDiscussionMessageEntity>
{
    public void Configure(EntityTypeBuilder<ItemDiscussionMessageEntity> builder)
    {
        builder.HasKey(idm => idm.Id);
        builder.HasOne(idm => idm.Item).WithMany(i => i.DiscussionMessages).HasForeignKey(idm => idm.ItemId)
            .IsRequired();
        builder.HasOne(idm => idm.User).WithMany(u => u.ItemDiscussionMessages).HasForeignKey(idm => idm.UserId)
            .IsRequired();
        builder.Property(idm => idm.Message).HasMaxLength(10240).IsRequired();
    }
}