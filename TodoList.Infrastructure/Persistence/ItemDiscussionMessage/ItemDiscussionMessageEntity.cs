﻿using TodoList.Infrastructure.Persistence.Item;
using TodoList.Infrastructure.Persistence.User;

namespace TodoList.Infrastructure.Persistence.ItemDiscussionMessage;

public class ItemDiscussionMessageEntity
{
    public int Id { get; set; }
    public int ItemId { get; set; }
    public int UserId { get; set; }
    public string Message { get; set; }
    public DateTime CreateDate { get; set; }

    public ItemEntity Item { get; set; }
    public UserEntity User { get; set; }
}