﻿using TodoList.Infrastructure.Persistence.User;
using TodoList.Infrastructure.Persistence.UserStory;

namespace TodoList.Infrastructure.Persistence.UserStoryDiscussionMessage;

public class UserStoryDiscussionMessageEntity
{
    public int Id { get; set; }
    public int UserStoryId { get; set; }
    public int UserId { get; set; }
    public string Message { get; set; }
    public DateTime CreateDate { get; set; }

    public UserStoryEntity UserStory { get; set; }
    public UserEntity User { get; set; }
}