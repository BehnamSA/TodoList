﻿using Microsoft.EntityFrameworkCore;

namespace TodoList.Infrastructure.Persistence.UserStoryDiscussionMessage;

public class UserStoryDiscussionMessageSeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<UserStoryDiscussionMessageEntity>().HasData(new[]
        {
            new UserStoryDiscussionMessageEntity
            {
                Id = 1,
                UserStoryId = 3,
                UserId = 2,
                Message = "I need more documents and business logic about ledgers in Accounting",
                CreateDate = DateTime.Now.AddMinutes(-30)
            }
        });
    }
}