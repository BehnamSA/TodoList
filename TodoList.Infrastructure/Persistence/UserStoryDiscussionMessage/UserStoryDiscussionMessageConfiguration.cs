﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.Infrastructure.Persistence.UserStoryDiscussionMessage;

public class UserStoryDiscussionMessageConfiguration : IEntityTypeConfiguration<UserStoryDiscussionMessageEntity>
{
    public void Configure(EntityTypeBuilder<UserStoryDiscussionMessageEntity> builder)
    {
        builder.HasKey(usd => usd.Id);
        builder.HasOne(usd => usd.UserStory).WithMany(i => i.DiscussionMessages).HasForeignKey(usd => usd.UserStoryId)
            .IsRequired();
        builder.HasOne(usd => usd.User).WithMany(u => u.UserStoryDiscussionMessages).HasForeignKey(usd => usd.UserId)
            .IsRequired();
        builder.Property(usd => usd.Message).HasMaxLength(10240).IsRequired();
    }
}