﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.Infrastructure.Persistence.ProjectUser;

public class ProjectUserConfiguration : IEntityTypeConfiguration<ProjectUserEntity>
{
    public void Configure(EntityTypeBuilder<ProjectUserEntity> builder)
    {
        builder.HasKey(pu => pu.Id);
        builder.HasOne(pu => pu.User).WithMany(u => u.ProjectUsers).HasForeignKey(pu => pu.UserId).IsRequired();
        builder.HasOne(pu => pu.Project).WithMany(p => p.ProjectUsers).HasForeignKey(pu => pu.ProjectId).IsRequired();
    }
}