﻿using Microsoft.EntityFrameworkCore;

namespace TodoList.Infrastructure.Persistence.ProjectUser;

public class ProjectUserSeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ProjectUserEntity>().HasData(new[]
        {
            new ProjectUserEntity
            {
                Id = 1,
                ProjectId = 1,
                UserId = 2
            }
        });
    }
}