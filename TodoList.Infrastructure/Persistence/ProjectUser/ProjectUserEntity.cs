﻿using TodoList.Infrastructure.Persistence.Project;
using TodoList.Infrastructure.Persistence.User;

namespace TodoList.Infrastructure.Persistence.ProjectUser;

public class ProjectUserEntity
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public int ProjectId { get; set; }

    public UserEntity User { get; set; }
    public ProjectEntity Project { get; set; }
}