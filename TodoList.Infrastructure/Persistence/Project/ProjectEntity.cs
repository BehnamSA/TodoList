﻿using TodoList.Infrastructure.Persistence.ProjectUser;
using TodoList.Infrastructure.Persistence.Sprint;
using TodoList.Infrastructure.Persistence.User;

namespace TodoList.Infrastructure.Persistence.Project;

public class ProjectEntity
{
    public int Id { get; set; }
    public int OwnerUserId { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }

    public UserEntity OwnerUser { get; set; }
    public List<ProjectUserEntity> ProjectUsers { get; set; } = new();
    public List<SprintEntity> Sprints { get; set; } = new();
}