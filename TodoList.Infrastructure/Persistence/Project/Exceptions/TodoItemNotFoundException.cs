﻿namespace TodoList.Infrastructure.Persistence.Project.Exceptions;

public class TodoItemNotFoundException : Exception
{
    public TodoItemNotFoundException() : base(message: "Todo item not found")
    {
    }
}