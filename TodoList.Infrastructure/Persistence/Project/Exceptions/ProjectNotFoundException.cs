﻿namespace TodoList.Infrastructure.Persistence.Project.Exceptions;

public class ProjectNotFoundException : Exception
{
    public ProjectNotFoundException() : base(message: "Project not found")
    {
    }
}