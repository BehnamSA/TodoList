﻿using Microsoft.EntityFrameworkCore;

namespace TodoList.Infrastructure.Persistence.Project;

public class ProjectSeeder
{
    public static void Seed(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ProjectEntity>().HasData(new[]
        {
            new ProjectEntity
            {
                Id = 1,
                OwnerUserId = 1,
                Title = "Accounting Application",
            },
            new ProjectEntity
            {
                Id = 2,
                OwnerUserId = 1,
                Title = "Financial Statement Tree Application",
            }
        });
    }
}