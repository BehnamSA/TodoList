﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TodoList.Infrastructure.Persistence.Project;

public class ProjectConfiguration : IEntityTypeConfiguration<ProjectEntity>
{
    public void Configure(EntityTypeBuilder<ProjectEntity> builder)
    {
        builder.HasKey(pu => pu.Id);
        builder.HasOne(pu => pu.OwnerUser).WithMany(u => u.OwnedProjects).HasForeignKey(pu => pu.OwnerUserId).IsRequired();
        builder.Property(pu => pu.Title).HasMaxLength(128).IsRequired();
        builder.Property(pu => pu.Description).HasMaxLength(512);
    }
}