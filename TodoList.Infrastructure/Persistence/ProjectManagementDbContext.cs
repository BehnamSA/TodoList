﻿using Microsoft.EntityFrameworkCore;
using TodoList.Infrastructure.Persistence.Item;
using TodoList.Infrastructure.Persistence.ItemDiscussionMessage;
using TodoList.Infrastructure.Persistence.Project;
using TodoList.Infrastructure.Persistence.ProjectUser;
using TodoList.Infrastructure.Persistence.Sprint;
using TodoList.Infrastructure.Persistence.User;
using TodoList.Infrastructure.Persistence.UserStory;
using TodoList.Infrastructure.Persistence.UserStoryDiscussionMessage;

namespace TodoList.Infrastructure.Persistence;

public class ProjectManagementDbContext : DbContext
{
    public ProjectManagementDbContext(DbContextOptions<ProjectManagementDbContext> options) : base(options)
    {
    }

    public DbSet<UserEntity> Users { get; set; }
    public DbSet<ProjectEntity> Projects { get; set; }
    public DbSet<ProjectUserEntity> ProjectUsers { get; set; }
    public DbSet<UserStoryEntity> UserStories { get; set; }
    public DbSet<ItemEntity> Items { get; set; }
    public DbSet<ItemDiscussionMessageEntity> ItemDiscussions { get; set; }
    public DbSet<UserStoryDiscussionMessageEntity> UserStoryDiscussions { get; set; }
    public DbSet<SprintEntity> Sprints { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserStoryConfiguration).Assembly);

        UserSeeder.Seed(modelBuilder);
        ProjectSeeder.Seed(modelBuilder);
        ProjectUserSeeder.Seed(modelBuilder);

        SprintSeeder.Seed(modelBuilder);

        UserStorySeeder.Seed(modelBuilder);
        UserStoryDiscussionMessageSeeder.Seed(modelBuilder);

        ItemSeeder.Seed(modelBuilder);
        ItemDiscussionMessageSeeder.Seed(modelBuilder);
    }
}