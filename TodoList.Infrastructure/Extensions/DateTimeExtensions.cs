﻿namespace TodoList.Infrastructure.Extensions;

public static class DateTimeExtensions
{
    public static string ToStringDateOnly(this DateTime value, string format = "yyyy-MM-dd") => value.ToString(format);
}