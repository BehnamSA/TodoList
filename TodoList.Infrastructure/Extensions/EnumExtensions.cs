﻿namespace TodoList.Infrastructure.Extensions;

public static class EnumExtensions
{
    public static int ToInt<TEnum>(this TEnum value) where TEnum : Enum => Convert.ToInt32(value);
}