﻿using System.Text;
using Newtonsoft.Json;

namespace TodoList.Infrastructure.Extensions;

public static class ObjectExtensions
{
    public static string? ToBase64<T>(this T? @object) where T : class
    {
        if (@object is null) return null;

        return Convert.ToBase64String(
            Encoding.UTF8.GetBytes(@object.ToString()!)
        );
    }

    public static string? SerializeToJson<T>(this T? @object)
    {
        if (@object is null) return null;

        return JsonConvert.SerializeObject(@object);
    }
}