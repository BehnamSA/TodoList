﻿using System.Text;

namespace TodoList.Infrastructure.Extensions;

public static class StringExtensions
{
    public static string? DecodeBase64(this string? value)
    {
        if (string.IsNullOrWhiteSpace(value))
            return null;

        var bytes = Convert.FromBase64String(value);

        return Encoding.UTF8.GetString(bytes);
    }

    public static bool IsBase64(this string? value)
    {
        try
        {
            value.DecodeBase64();
        }
        catch (FormatException)
        {
            return false;
        }

        return true;
    }

    public static int ToInt(this string value) => int.Parse(value);

    public static DateTime ToDateTime(this string value) => DateTime.Parse(value);

    public static bool ToBool(this string value) => bool.Parse(value);
}