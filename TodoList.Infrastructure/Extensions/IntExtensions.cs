﻿namespace TodoList.Infrastructure.Extensions;

public static class IntExtensions
{
    public static TEnum ToEnum<TEnum>(this int input) where TEnum : Enum
        => (TEnum)Enum.Parse(typeof(TEnum), input.ToString(), true);
}